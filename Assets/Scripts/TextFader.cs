﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextFader : MonoBehaviour
{
    public float speed;
    public bool correct;
    public GameObject correctObj;
    public GameObject incorrectObj;
    public Text text;
    public Vector2 spawnPos;

    private Color color;
    private GameObject curObj;

	void Start()
	{
        if (correct)
        {
            correctObj.SetActive(true);
            text = correctObj.GetComponent<Text>();
            curObj = correctObj;
        }
        else
        {
            incorrectObj.SetActive(true);
            text = incorrectObj.GetComponent<Text>();
            curObj = incorrectObj;
        }

        curObj.transform.position = spawnPos;
	}
	
	void Update()
	{
        curObj.transform.Translate(Vector2.up * speed * Time.deltaTime * 20);
        color = text.color;
        color.a -= Time.deltaTime;
        text.color = color;

        if (color.a <= 0)
            Destroy(gameObject);
	}
}