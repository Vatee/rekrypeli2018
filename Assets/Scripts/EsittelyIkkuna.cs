﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Runtime.InteropServices;

public class EsittelyIkkuna : MonoBehaviour {
    
    public GameObject infoIkkuna;
    public Text titleText;    
    public GameObject videoButton;
    public Text tutkintoText;
    public TMPro.TextMeshProUGUI titleText2;
    public Text descriptionText;
    private KoulutusalaEsittely currentKAE;
    public KoulutusalaEsittely[] kAlaEsittelyt;

    public void OpenWindow(GameObject button)
    {      
        foreach (KoulutusalaEsittely kAlaEsittely in kAlaEsittelyt)
        {
            if (button.GetComponentInChildren<Text>(true).text.Contains(kAlaEsittely.kAla.ToDescription()))
            {       
                titleText.text = kAlaEsittely.kAla.ToDescription();
                tutkintoText.text = kAlaEsittely.tutkinto;
                titleText2.text = kAlaEsittely.kAla.ToDescription();
                descriptionText.text = kAlaEsittely.description;
                currentKAE = kAlaEsittely;

                if (currentKAE.videoUrl == "")
                    videoButton.SetActive(false);
                else
                    videoButton.SetActive(true);

                infoIkkuna.SetActive(true);
            }
        }
    }

    public void OpenUrl(string url)
    {
        #if UNITY_WEBGL && !UNITY_EDITOR
            openWindow(url);
        #else
            Application.OpenURL(url);
        #endif        
    }

    public void OpenLink()
    {
        if (currentKAE.websiteUrl != null)
        {
            OpenUrl(currentKAE.websiteUrl);
        }
    }

    public void PlayVideo()
    {
        if (currentKAE.videoUrl != "")
        {
            OpenUrl(currentKAE.videoUrl);
        }
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}
