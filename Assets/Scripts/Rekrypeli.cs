﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;


[System.Serializable]
public class ButtonInfo
{
    public Button button;
    public Text textField;
    public TextMeshProUGUI textField2;
    public Vastaus vastaus;
    public bool selected;
}

public class Rekrypeli : MonoBehaviour
{
    public Transform buttonsObj;
    private List<ButtonInfo> buttons = new List<ButtonInfo>();

    public GameObject confirmButton;
    
    public Kysymys[] kysymykset;
    private Kysymys kysymys;

    public Text kysymysText;
    public TextMeshProUGUI kysymysText2;

    public GameObject results;
    public GameObject game;

    public int questionsCount;
    private int answeredCount;

    private Dictionary<Koulutusala, float> answerCounter = new Dictionary<Koulutusala, float>();

    int game1start;
    int game2start;
    int game3start;

    UnityEngine.EventSystems.EventSystem eventSystem;
    FlexibleSpacing fSpace;

    private void Awake()
    {
        game1start = Random.Range(2, 5);
        game2start = Random.Range(6, 9);
        game3start = Random.Range(12, 14);

        eventSystem = FindObjectOfType<UnityEngine.EventSystems.EventSystem>();
        fSpace = FindObjectOfType<FlexibleSpacing>();
    }

    // Use this for initialization
    void Start()
    {
        CreateAnswerCounters();
        FindButtons();
        NextQuestion();
    }

    void CreateAnswerCounters()
    {
        foreach (Koulutusala koulutusala in System.Enum.GetValues(typeof(Koulutusala)))
        {
            answerCounter.Add(koulutusala, 0);
        }
    }

    void FindButtons()
    {
        for (int i = 0; i < buttonsObj.childCount; i++)
        {
            ButtonInfo tempBI = new ButtonInfo();
            tempBI.button = buttonsObj.GetChild(i).GetComponent<Button>();
            tempBI.textField = buttonsObj.GetChild(i).GetComponentInChildren<Text>(true);
            tempBI.textField2 = buttonsObj.GetChild(i).GetComponentInChildren<TextMeshProUGUI>(true);
            buttons.Add(tempBI);
            buttonsObj.GetChild(i).gameObject.SetActive(false);
        }
    }


    /*
    private int QuestionCount(Koulutusala ka)
    {
        int qCount = 0;        

        foreach (Kysymys kys in kysymykset)
        {
            if (kys.kAla.HasFlag(ka))
                qCount++;
        }

        return qCount;
    } */
    int currentTier = 3;
    KeyValuePair<Koulutusala, float> kvp = new KeyValuePair<Koulutusala, float>(0, 0);

    public void NextQuestion()
    {
        List<Kysymys> tempKysList = FillList();

        currentTier = Mathf.RoundToInt(3 - kvp.Value);
        if (currentTier < 0)
            currentTier = 0;        

        int questionCount = kysymykset.Where(x => x.kAla.HasFlag(kvp.Key)).Count();

        if (answeredCount < questionsCount && kvp.Value < questionCount * 0.7)
        {
            int rndNum = Random.Range(0, tempKysList.Count - 1);
            kysymys = tempKysList[rndNum];

            kysymysText.text = kysymys.kysymys;
            kysymysText2.text = kysymys.kysymys;

            int i = 0;
            foreach (ButtonInfo buttonInfo in buttons)
            {
                if (kysymys.vastaukset.Length > i)
                {
                    buttonInfo.vastaus = kysymys.vastaukset[i];
                    buttonInfo.textField.text = buttonInfo.vastaus.vastaus;
                    buttonInfo.textField2.text = buttonInfo.vastaus.vastaus;
                    buttonInfo.button.gameObject.SetActive(true);
                }
                else
                {
                    buttonInfo.button.gameObject.SetActive(false);
                }
                i++;
            }

            if (kysymys.monivalinta)
                confirmButton.SetActive(true);
            else
                confirmButton.SetActive(false);

            kysymys.used = true;

            fSpace.ButtonsChanged();

            if (answeredCount == game1start)
                MinigamesMaster.GetInstance.LoadScene(1);

            if (answeredCount == game2start)
                MinigamesMaster.GetInstance.LoadScene(2);

            if (answeredCount == game3start)
                MinigamesMaster.GetInstance.LoadScene(3);
        }
        else
        {
            PrintResults();
        }
    }

    List<Kysymys> FillList()
    {
        List<Kysymys> tempKysList = new List<Kysymys>();
        int i = 0;
        while (tempKysList.Count <= 0 && i < answerCounter.Count)
        {
            kvp = answerCounter.OrderByDescending(x => x.Value).ElementAt(i);
            if (kvp.Value == 0)
                tempKysList = kysymykset.Where(x => !x.used && x.tier == currentTier).ToList();
            else
                tempKysList = kysymykset.Where(x => x.kAla.HasFlag(kvp.Key) && !x.used && x.tier == currentTier).ToList();

            if (currentTier > 0 && tempKysList.Count == 0)
                currentTier--;
            else if (tempKysList.Count == 0)
            {
                currentTier = 3;
                i++;
            }                
        }

        return tempKysList;
    }



    /*public void Shuffle()
    {
        for (int i = 0; i < kysymykset.Length; i++)
        {
            int rnd = Random.Range(0, kysymykset.Length);
            Kysymys tempGO = kysymykset[rnd];
            kysymykset[rnd] = kysymykset[i];
            kysymykset[i] = tempGO;
        }
    } */


    void PrintResults()
    {
        if (Debug.isDebugBuild)
            Debug.Log("Tulokset: ");

        results.SetActive(true);
        game.SetActive(false);

        GameObject[] gObjs = GameObject.FindGameObjectsWithTag("Koulutusala");

        for (int i = 0; i < 3; i++)
        {
            KeyValuePair<Koulutusala, float> kvp = answerCounter.OrderByDescending(x => x.Value).ElementAt(i);
            if (kvp.Value != 0)
            {
                if (Debug.isDebugBuild)
                    Debug.Log(kvp.Key + ": " + kvp.Value);
                gObjs[i].GetComponent<Text>().text = kvp.Key.ToDescription();
            }
            else
                gObjs[i].transform.parent.gameObject.SetActive(false);
        }
    }
    
    public void Answer(Button button)
    {
        if (kysymys.monivalinta)
            SelectButton(button);        
        else
        {
            AnswerCounter(button);
            answeredCount++;
            NextQuestion();            
        }
    }

    public void ConfirmButton(Button button)
    {        
        foreach (ButtonInfo bI in buttons)
        {
            if (bI.selected)
            {
                if (button.tag == "ConfirmButton")                    
                    AnswerCounter(bI.button);

                ColorBlock colors = bI.button.colors;
                colors.normalColor = Color.white;
                colors.highlightedColor = Color.white;
                bI.button.colors = colors;

                bI.selected = false;
            }
        }               

        answeredCount++;
        NextQuestion();
    }

    void SelectButton(Button button)
    {
        ColorBlock colors = button.colors;
        ButtonInfo bInfo = buttons.Where(x => x.button == button).Single();

        bInfo.selected = !bInfo.selected;

        if (bInfo.selected)
        {
            Color color = new Color32(200, 200, 200, 255);
            colors.normalColor = color;
            colors.highlightedColor = color;
        }
        else
        {
            colors.normalColor = Color.white;
            colors.highlightedColor = Color.white;
        }

        button.colors = colors;

        eventSystem.SetSelectedGameObject(null);
    }

    void AnswerCounter(Button button)
    {
        ButtonInfo bInfo = buttons.Where(x => x.button == button).Single();
        foreach (KoulAlaValinta kAla in bInfo.vastaus.kAla)
        {
            answerCounter[kAla.koulutusala] += kAla.amount;

            if (answerCounter[kAla.koulutusala] < 0)
                answerCounter[kAla.koulutusala] = 0;
        }
    }

}
