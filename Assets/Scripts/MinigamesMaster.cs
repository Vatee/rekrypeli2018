﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MinigamesMaster : Singleton<MinigamesMaster> {   
    private int currentScene;
    public GameObject game;
    private AsyncOperation async;
    public GameObject loadingScreen;

    public GameObject skipButton;
    

    // Use this for initialization
    void Start ()
    {
        Application.targetFrameRate = 60;
        async = LoadSceneAsync(1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown("s") && !game.activeSelf)
            CloseScene();        
    }

    public void LoadScene(int index)
    {
        game.SetActive(false);
        async.allowSceneActivation = true;
        currentScene = index;

        skipButton.SetActive(true);
        if (!loadingScreen.activeSelf)
            loadingScreen.SetActive(true);
    } 

    public void CloseScene()
    {
        if (SceneManager.GetActiveScene().buildIndex == currentScene)
        {
            SceneManager.UnloadSceneAsync(currentScene);
            game.SetActive(true);
            skipButton.SetActive(false);

            if (SceneUtility.GetScenePathByBuildIndex(currentScene + 1).Length > 0)
                async = LoadSceneAsync(currentScene + 1);
        }

        BackgroundSwapper.GetInstance.RandomBackground();
    }

    public AsyncOperation LoadSceneAsync(int newSceneID)
    {        
        AsyncOperation async = SceneManager.LoadSceneAsync(newSceneID, LoadSceneMode.Additive);
        async.allowSceneActivation = false;

        async.completed += (AsyncOperation async2) =>
        {
            if (SceneManager.GetSceneByBuildIndex(newSceneID).IsValid())
                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(newSceneID));

            if (loadingScreen.activeSelf)
                loadingScreen.SetActive(false);
        };

        return async;
    }
}
