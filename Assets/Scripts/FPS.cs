﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPS : MonoBehaviour
{
    public float updateInterval;
    public Text fpsText;
    public TMPro.TextMeshProUGUI fpsText2;

    private float accum = 0f;
    private int frames = 0;
    private float timeLeft;
    private float fps;


    private void Awake()
    {
        if (!Debug.isDebugBuild)
            gameObject.SetActive(false);
    }

    private void Start()
    {
        timeLeft = updateInterval;
    }

    void Update()
	{
        timeLeft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        frames++;

        if(timeLeft < 0)
        {
            fps = accum / frames;
            if (fpsText)
                fpsText.text = "FPS: " + Mathf.FloorToInt(fps).ToString();
            else if (fpsText2)
                fpsText2.text = "FPS: " + Mathf.FloorToInt(fps).ToString();

            timeLeft = updateInterval;
            accum = 0f;
            frames = 0;
        }
	}
}
