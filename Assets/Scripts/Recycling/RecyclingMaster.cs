﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TrashType
{
    Biojate,
    Energiajate,
    Sekajate,
    Lasi,
    Paperi,
    Kartonki,
    Metalli,
    Sahko,
    Vaarallinen
}

public class RecyclingMaster : MonoBehaviour
{
    private GameObject[] trashes;
    private int trashesCounter = 0;
    private int tries = 0;
    public GameObject resultsObj;
    private RecyclingResults results;
    public bool gameStopped;
    public int seconds;
    public int wrongAmount;
    public GameObject introPanel;
    public GameObject trashesParent;

    private void Awake()
    {
        trashes = GameObject.FindGameObjectsWithTag("Trash");
        results = GameObject.FindObjectOfType<RecyclingResults>();
    }

    private void Start()
    {
        Time.timeScale = 0;
        trashesParent.SetActive(false);
        if (!introPanel.activeSelf)
            introPanel.SetActive(true);

    }

    public void Recycled(GameObject trash)
    {
        tries++;
        trashesCounter++;
        Destroy(trash);
        if (trashesCounter == trashes.Length)
        {
            EndSorting();
        }
    }

    public void EndSorting()
    {
        gameStopped = true;
        print("Lajiteltu!");
        results.UpdateResults();
        //yield return new WaitForSeconds(1f);        
        resultsObj.SetActive(true);        
    }

    public void StartGame()
    {
        StartCoroutine(Clock());
        Time.timeScale = 1;
        introPanel.SetActive(false);
        trashesParent.SetActive(true);
    }

    IEnumerator Clock()
    {
        while (!gameStopped)
        {
            yield return new WaitForSecondsRealtime(1);
            seconds++;
        }
        //return null;
    }

    public void CloseGame()
    {
        if (MinigamesMaster.GetInstance)
            MinigamesMaster.GetInstance.CloseScene();
    }
}
