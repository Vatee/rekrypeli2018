﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour {
    public TrashType trashType;
    private GameObject canvasCorrect;
    private RecyclingMaster master;
    //private RecyclingResults results;

    private void Awake()
    {
        canvasCorrect = Resources.Load<GameObject>("CanvasCorrect");
        master = GameObject.FindObjectOfType<RecyclingMaster>();
        //results = GameObject.FindObjectOfType<RecyclingResults>();
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.parent.tag == "Trash")
        {
            if (other.GetComponentInParent<Trash>().type == trashType)
            {
                Correct(true, transform.position);
                master.Recycled(other.transform.parent.gameObject);
            }
            else
            {
                Correct(false, transform.position);
                master.wrongAmount++;
            }                
        }
    }

    private void Correct(bool wasCorrect, Vector2 position)
    {
        if (wasCorrect)
        {
            GameObject tempObj = Instantiate(canvasCorrect);
            tempObj.transform.position = new Vector2(position.x, position.y + 2f);
            tempObj.GetComponent<TextFaderRecycling>().correct = true;            
        }
        else
        {
            GameObject tempObj = Instantiate(canvasCorrect);
            tempObj.transform.position = new Vector2(position.x, position.y + 2f);
            tempObj.GetComponent<TextFaderRecycling>().correct = false;
        }
    }
}
