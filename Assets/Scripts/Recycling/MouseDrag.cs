﻿using UnityEngine;
using System.Collections;

public class MouseDrag : MonoBehaviour
{
    //public Transform rootObj;
    public Transform transformToMove;    
    private GameObject transformToMoveCollider;
    private SpriteRenderer transformToMoveSprite;
    public LayerMask raycastMask;

    [SerializeField]
    private Vector3 mousePos;
    private RaycastHit2D hit;

    private Vector2 lastPos;
    private Vector2 velocity;

    public Vector2 edgesMin;
    public Vector2 edgesMax;

    private Trash transformToMoveTrash;
    private Rigidbody2D rb2d;
    
    void Start()
    {
        //edgesMin.x += rootObj.position.x;
        //edgesMax.x += rootObj.position.x;
        //edgesMin.y += rootObj.position.y;
        //edgesMax.y += rootObj.position.y;
    }

    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            hit = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity, raycastMask);

            if (!transformToMove)
            {
                if (hit.transform)
                {
                    transformToMove = hit.transform;
                    transformToMoveTrash = transformToMove.GetComponent<Trash>();
                    transformToMoveTrash.grabbed = true;
                    transformToMoveCollider = transformToMove.GetChild(0).gameObject;
                    transformToMoveCollider.layer = LayerMask.NameToLayer("Drag");
                    transformToMoveSprite = transformToMove.GetComponent<SpriteRenderer>();
                    transformToMoveSprite.sortingOrder = 3;
                    lastPos = transformToMove.position;
                    rb2d = transformToMove.GetComponent<Rigidbody2D>();
                    rb2d.isKinematic = true;
                    rb2d.velocity = Vector2.zero;
                }
            }  
            else if (transformToMove)                
            {
                mousePos.z = transformToMove.position.z;
                if (lastPos == Vector2.zero)
                    lastPos = transformToMove.position;
                transformToMove.position = new Vector2(Mathf.Clamp(mousePos.x, edgesMin.x, edgesMax.x), Mathf.Clamp(mousePos.y, edgesMin.y, edgesMax.y));

                velocity = new Vector2((transformToMove.position.x - lastPos.x), (transformToMove.position.y - lastPos.y));
                lastPos = transformToMove.position;
            }
        }
    }
    /*
    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && transformToMove)
        {
            mousePos.z = transformToMove.position.z;
            lastPos = transformToMove.position;
            transformToMove.position = new Vector2(Mathf.Clamp(mousePos.x, edgesMin.x, edgesMax.x), Mathf.Clamp(mousePos.y, edgesMin.y, edgesMax.y));
            //transformToMove.position = Vector2.Lerp(transformToMove.position, mousePos, Time.fixedDeltaTime);

            //print(lastPos.x);
            velocity = new Vector2((transformToMove.position.x - lastPos.x) / Time.fixedDeltaTime, (transformToMove.position.y - lastPos.y) / Time.fixedDeltaTime);
            
        }

        
    } */

    void LateUpdate()
    {
        if (Input.GetMouseButtonUp(0) && transformToMove)
        {
            //print(velocity);
            rb2d.isKinematic = false;
            //rb2d.velocity = velocity * 0.2f;
            rb2d.velocity = velocity / Time.deltaTime * 0.2f;
            //transformToMove.position = new Vector3(mousePos.x, mousePos.y, 0f);
            //transformToMoveCollider.SetActive(true);

            if (!transformToMoveTrash.onTop)
            {
                transformToMoveCollider.layer = 0;
                transformToMoveSprite.sortingOrder = 0;
            }

            transformToMoveTrash.grabbed = false;
            transformToMove = null;
            transformToMoveCollider = null;
        }
    }
}