﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AddColliderScripts : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (GameObject gObj in GameObject.FindGameObjectsWithTag("Collector"))
        {
            foreach (Collector col in gObj.GetComponents<Collector>())
            {
                DestroyImmediate(col);
            }

            gObj.AddComponent<Collector>();
        }
        print("Added collector scripts");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
