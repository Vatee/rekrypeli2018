﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFaderRecycling : MonoBehaviour {
    public float speed;
    public bool correct;
    public GameObject correctObj;
    public GameObject incorrectObj;
    public Text text;

    private Color color;

    void Start()
    {
        if (correct)
        {
            correctObj.SetActive(true);
            text = correctObj.GetComponent<Text>();
        }
        else
        {
            incorrectObj.SetActive(true);
            text = incorrectObj.GetComponent<Text>();
        }
    }

    void Update()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
        color = text.color;
        color.a -= Time.deltaTime;
        text.color = color;

        if (color.a <= 0)
            Destroy(gameObject);
    }
}
