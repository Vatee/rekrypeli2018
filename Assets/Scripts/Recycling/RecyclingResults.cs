﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecyclingResults : MonoBehaviour {
    //public float seconds;
    //public int wrongAmount;
    private float points;

    public Text secondsText;
    public Text wrongText;
    public Text pointsText;

    public Text timeValue;

    public RectTransform ratingStars;
    private int starsWidth;
    private int starsMaxWidth;
    private GameObject[] trashes;
    private RecyclingMaster rcMaster;

    //private bool gameStopped;
    private void Awake()
    {
        trashes = GameObject.FindGameObjectsWithTag("Trash");
    }

    // Use this for initialization
    void Start () {
        starsMaxWidth = Mathf.RoundToInt(ratingStars.sizeDelta.x);        
        rcMaster = GameObject.FindObjectOfType<RecyclingMaster>();
    }
	
	// Update is called once per frame
	void Update () {
        //seconds++;
        timeValue.text = rcMaster.seconds.ToString() + " s";
	}    

    public void UpdateResults()
    {
        secondsText.text = Mathf.RoundToInt(rcMaster.seconds).ToString() + " s";
        wrongText.text = rcMaster.wrongAmount.ToString();
        points = (trashes.Length*1.5f)/(rcMaster.seconds + rcMaster.wrongAmount) * starsMaxWidth;
        pointsText.text = Mathf.RoundToInt(points).ToString();

        starsWidth = Mathf.RoundToInt(points);
        if (starsWidth > 500)
            starsWidth = 500;

        ratingStars.sizeDelta = new Vector2(starsWidth, ratingStars.sizeDelta.y);
        print("starsWidth: " + starsWidth + "/" + starsMaxWidth);
    }
}
