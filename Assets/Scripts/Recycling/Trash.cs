﻿using UnityEngine;
using System.Collections;

public class Trash : MonoBehaviour
{
    public TrashType type;
    private RecyclingMaster master;
    public bool grabbed;
    public bool onTop;

    private void Awake()
    {

    }

    private void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (!onTop)
            onTop = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        onTop = false;

        if (!grabbed) 
        {
            transform.GetChild(0).gameObject.layer = 0;
            transform.GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
    }
}