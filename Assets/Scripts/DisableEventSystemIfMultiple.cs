﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableEventSystemIfMultiple : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        if (GameObject.FindObjectsOfType<UnityEngine.EventSystems.EventSystem>().Length > 1)
            this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
