﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VaaramerkitResults : MonoBehaviour {

    public float seconds;
    public int wrongAmount;
    private float points;

    public Text secondsText;
    public Text wrongText;
    public Text pointsText;

    public Text timeValue;
    public Text wrongValue;

    public RectTransform ratingStars;
    private int starsWidth;
    private int starsMaxWidth;

    private bool gameStopped;

    public GameObject resultsScreen;
    private VaaramerkitMaster master;

    public GameObject hide;

    private void Awake()
    {
        master = GameObject.FindObjectOfType<VaaramerkitMaster>().GetComponent<VaaramerkitMaster>();
        starsMaxWidth = Mathf.RoundToInt(ratingStars.sizeDelta.x);
    }

    // Use this for initialization
    void Start()
    {
        hide.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //seconds++;
        if (timeValue.text != seconds.ToString() + " s")
            timeValue.text = seconds.ToString() + " s";
        if (wrongValue.text != wrongAmount.ToString())
            wrongValue.text = wrongAmount.ToString();
    }

    public void UpdateResults()
    {
        gameStopped = true;
        secondsText.text = Mathf.RoundToInt(seconds).ToString() + " s";
        wrongText.text = wrongAmount.ToString();
        points = (master.vaaramerkit.Length * 2f - wrongAmount) / (seconds + wrongAmount * 3) * starsMaxWidth;
        pointsText.text = Mathf.RoundToInt(points).ToString();

        starsWidth = Mathf.RoundToInt(points);
        if (starsWidth > 500)
            starsWidth = 500;
        else if (starsWidth < 0)
            starsWidth = 0;

        ratingStars.sizeDelta = new Vector2(starsWidth, ratingStars.sizeDelta.y);
        print("starsWidth: " + starsWidth + "/" + starsMaxWidth);

        resultsScreen.SetActive(true);
        hide.SetActive(false);
    }

    public void StartClock()
    {
        StartCoroutine(Clock());
        hide.SetActive(true);
    }

    IEnumerator Clock()
    {
        while (!gameStopped)
        {
            yield return new WaitForSecondsRealtime(1);
            seconds++;
        }
        //return null;
    }
}
