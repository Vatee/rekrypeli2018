﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class Vaaramerkki
{
    private int id;
    public string name;    
    //public string description;
    public Sprite sprite;
}

public class VaaramerkitMaster : MonoBehaviour {
    public Vaaramerkki[] vaaramerkit;
    private Vaaramerkki merkki;

    public Image img;
    public Text[] textboxes;

    private int nextId = 0;

    private GameObject canvasCorrect;
    private VaaramerkitResults results;

    public GameObject introPanel;

    private void Awake()
    {
        canvasCorrect = Resources.Load<GameObject>("CanvasCorrectVaaramerkit");
        results = GameObject.FindObjectOfType<VaaramerkitResults>();
    }

    // Use this for initialization
    void Start ()
    {
        Shuffle();        

        Time.timeScale = 0;
        if (!introPanel.activeSelf)
            introPanel.SetActive(true);
    }
	
	// Update is called once per frame
	void Update ()
    {

    }
    
    void RandomSign()
    {
        merkki = vaaramerkit[Random.Range(0, vaaramerkit.Length)];
        img.sprite = merkki.sprite;       

        Text rndTextbox = textboxes[Random.Range(0, textboxes.Length)];
        rndTextbox.text = merkki.name;        
       
        foreach (Text textbox in textboxes)
        {
            if (textbox != rndTextbox)
                textbox.text = SetTextBoxText();
        }
    } 

    void NextSign()
    {
        //print(nextId);
        if (nextId < vaaramerkit.Length)
        {
            merkki = vaaramerkit[nextId];
            img.sprite = merkki.sprite;

            //int textNumber = Random.Range(0, textboxes.Length);
            Text rndTextbox = textboxes[Random.Range(0, textboxes.Length)];
            rndTextbox.text = merkki.name;

            foreach (Text textbox in textboxes)
            {
                if (textbox != rndTextbox)
                    textbox.text = SetTextBoxText();
            }

            nextId++;
        }
        else
            results.UpdateResults();            
    }

    private string SetTextBoxText()
    {
        bool available = false;
        int i = 0;

        while (!available)
        {
            i = Random.Range(0, vaaramerkit.Length);
            available = true;

            foreach (Text textbox in textboxes)
            {
                if (textbox.text == vaaramerkit[i].name)
                    available = false;
            }
        }     
        
        return vaaramerkit[i].name;
    }

    private void Shuffle()
    {
        for (int i = 0; i < vaaramerkit.Length; i++)
        {
            int rnd = Random.Range(0, vaaramerkit.Length);
            Vaaramerkki temp = vaaramerkit[rnd];
            vaaramerkit[rnd] = vaaramerkit[i];
            vaaramerkit[i] = temp;
        }
    }

    public void ChoiceButton(Text buttonText)
    {
        if (merkki.name == buttonText.text)
        {
            //RandomSign();
            NextSign();          
            Correct(true, buttonText.transform.position);
        }            
        else
        {
            results.wrongAmount++;
            Correct(false, buttonText.transform.position);
        }

        FindObjectOfType<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
    }

    private void Correct(bool wasCorrect, Vector2 position)
    {
        //print("Correct(" + wasCorrect + ", " + position + ")");
        if (wasCorrect)
        {
            GameObject tempObj = Instantiate(canvasCorrect);
            tempObj.GetComponent<TextFader>().spawnPos = new Vector2(position.x, position.y);
            tempObj.GetComponent<TextFader>().correct = true;
        }
        else
        {
            GameObject tempObj = Instantiate(canvasCorrect);
            tempObj.GetComponent<TextFader>().spawnPos = new Vector2(position.x, position.y);
            tempObj.GetComponent<TextFader>().correct = false;
        }
    }

    IEnumerator test()
    {
        while (true)
        {
            //RandomSign();
            NextSign();
            yield return new WaitForSecondsRealtime(2);
        }
    }

    public void CloseGame()
    {
        if (MinigamesMaster.GetInstance)
            MinigamesMaster.GetInstance.CloseScene();
    }

    public void StartGame()
    {
        NextSign();
        results.StartClock();
        introPanel.SetActive(false);
        Time.timeScale = 1;
    }
}
