﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerTest : MonoBehaviour {

    void Start()
    {
        // Will attach a VideoPlayer to the main camera.
        //GameObject camera = GameObject.Find("Main Camera");

        // VideoPlayer automatically targets the camera backplane when it is added
        // to a camera object, no need to change videoPlayer.targetCamera.
        //var videoPlayer = camera.AddComponent<UnityEngine.Video.VideoPlayer>();
        var videoPlayer = GetComponent<UnityEngine.Video.VideoPlayer>();
        // Play on awake defaults to true. Set it to false to avoid the url set
        // below to auto-start playback since we're in Start().
        videoPlayer.playOnAwake = false;

        // By default, VideoPlayers added to a camera will use the far plane.
        // Let's target the near plane instead.
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.RenderTexture;

        // This will cause our scene to be visible through the video being played.
        videoPlayer.targetCameraAlpha = 1F;

        // Set the video to play. URL supports local absolute or relative paths.
        // Here, using absolute.
        videoPlayer.url = "https://r4---sn-4g5ednsd.googlevideo.com/videoplayback?id=o-AMZBNTaaTNQ1qUqNXPHio_q0PjjQiLVaP5fZjDYUyB4o&c=WEB&expire=1520537799&ipbits=0&pl=19&signature=7662D7F0C2C7FA0DC33555AB9B3EDFFC6FD34C5A.10D8EE13090890697B585DC9B10414C7CECF8A39&mime=video%2Fmp4&requiressl=yes&ip=2001%3A19f0%3A7402%3A95%3A5400%3Aff%3Afe6a%3Ad50a&fvip=3&key=cms1&sparams=dur,ei,expire,id,ip,ipbits,itag,lmt,mime,mip,mm,mn,ms,mv,pl,ratebypass,requiressl,source&itag=22&lmt=1472303954546697&dur=119.884&ratebypass=yes&source=youtube&ei=ZzyhWrPBEYfqW7yLr5gI&mip=82.116.252.148&cm2rm=sn-av0ox5-u30e7s,sn-5gol77z&req_id=ac0bed6c8cefa3ee&redirect_counter=2&cms_redirect=yes&mm=34&mn=sn-4g5ednsd&ms=ltu&mt=1520516093&mv=m";

        // Skip the first 100 frames.
        videoPlayer.frame = 100;

        // Restart from beginning when done.
        videoPlayer.isLooping = true;

        // Each time we reach the end, we slow down the playback by a factor of 10.
        videoPlayer.loopPointReached += EndReached;

        videoPlayer.aspectRatio = UnityEngine.Video.VideoAspectRatio.FitHorizontally;

        // Start playback. This means the VideoPlayer may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.
        videoPlayer.Play();
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        vp.playbackSpeed = vp.playbackSpeed / 10.0F;
    }
}
