﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour {
    public GameObject menuPanel;
    public GameObject closeButton;

	public void OnClick()
    {
        menuPanel.SetActive(!menuPanel.activeSelf);
        closeButton.SetActive(menuPanel.activeSelf);
    }
}
