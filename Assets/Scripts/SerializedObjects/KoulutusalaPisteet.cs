﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KoulutusalaPisteet
{
    public Koulutusala koulutusala;
    public float pisteet;	
}
