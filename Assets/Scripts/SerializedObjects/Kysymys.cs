﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Kysymys
{
    public string kysymys;
    public int tier;
    //public Koulutusala[] koulutusalat;
    [EnumFlag] public Koulutusala kAla;
    public Vastaus[] vastaukset;

    //[HideInInspector]
    public bool used;
    public bool monivalinta;
}

