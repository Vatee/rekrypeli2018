﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Vastaus
{
    public string vastaus;
    //public Koulutusala[] koulutusalat;
    public KoulAlaValinta[] kAla;
    //public int amount;

    [HideInInspector]
    public bool used;
}

