﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;


//[System.Serializable]
[System.Flags]
public enum Koulutusala
{
    Datanomi            =      1, // Power of two
    Merkonomi           =      2,
    Lähihoitaja         =      4,
    Sähköasentaja       =      8,
    Ajoneuvoasentaja    =      16,
    Artesaani           =      32,
    Leipuri             =      64,
    [Description("Kokki/Tarjoilija")] Kokki = 128,
    Koneistaja          =      256,
    [Description("Mediapalveluiden tuottaja")] Media = 512,
    Autonkuljettaja     =      1024,
    Talonrakentaja      =      2048,
    Maalari             =      4096,
    Putkiasentaja       =      8192,
    Levyseppähitsaaja   =      16384,
    Suunnitteluassistentti =   32768
}