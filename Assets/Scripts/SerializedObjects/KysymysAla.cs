﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KysymysAla
{
    public string name;
    public Koulutusala kAla;
    public Kysymys[] kysymykset;    
}
