﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KoulAlaValinta
{
    public Koulutusala koulutusala;
    public float amount;	
}
