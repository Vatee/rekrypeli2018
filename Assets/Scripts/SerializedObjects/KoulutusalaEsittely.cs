﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

[System.Serializable]
public class KoulutusalaEsittely {
    public Koulutusala kAla;
    public string websiteUrl;
    public string videoUrl;
    public string tutkinto;
    public string description;    
}
