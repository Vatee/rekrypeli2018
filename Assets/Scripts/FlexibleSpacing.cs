﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlexibleSpacing : MonoBehaviour {
    VerticalLayoutGroup vGroup;
    //int oldCount = 0;
    //int newCount = 0;
    Button[] buttons;
    public TMPro.TextMeshProUGUI title;
    public Button confirmButton;

	// Use this for initialization
	void Awake () {
        vGroup = GetComponent<VerticalLayoutGroup>();
        buttons = GetComponentsInChildren<Button>(true);
	}
	/*
	// Update is called once per frame
	void Update () {
        newCount = GetActiveChildren();

        if (newCount != oldCount)
            SetSpacing(newCount);

        oldCount = newCount;
	} */

    int GetActiveChildren()
    {
        int childCount = 0;
        foreach (Button button in buttons)
        {
            if (button.IsActive())
                childCount++;
        }

        if (confirmButton.IsActive())
            childCount++;

        return childCount;
    }

    void SetSpacing(int buttonCount)
    {
        if (buttonCount > 4)
        {
            vGroup.spacing = 50 - (buttonCount - 4) * 10;
        }
        else
        {
            vGroup.spacing = 50;
        }

        SetButtonsHeight(buttonCount);    

    }

    void SetButtonsHeight(int buttonCount)
    {
        Vector2 scale = new Vector2(1000, 210 - (buttonCount * 20)); 
        foreach (Button button in buttons)
        {
            button.gameObject.GetComponent<RectTransform>().sizeDelta = scale;            
        }

        //confirmButton.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(250, 210 - (buttonCount * 20));
    }

    public void ButtonsChanged()
    {
        SetSpacing(GetActiveChildren());
        SetTitleSize();
    }

    void SetTitleSize()
    {
        title.fontSize = 80;
        title.ForceMeshUpdate();
        
        while (title.isTextTruncated)
        {
            title.fontSize -= 10;
            title.ForceMeshUpdate();
        }
    } 
}
