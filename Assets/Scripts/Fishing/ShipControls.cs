﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipControls : MonoBehaviour
{
    private FishMaster master;
    private FishData data;
    public Vector3 targetPos;

    [SerializeField]
    private float moveSpeed;
    private Vector3 movement;

    [SerializeField]
    private float rotationSpeed;

    [SerializeField]
    private Transform net;
    [SerializeField]
    private Transform netObj;
    [SerializeField]
    private float netCollectPosY;
    private int catchCount;
    private WaitForEndOfFrame wfeof;
    private List<GameObject> fishesInNet;
    public bool pullingNetUp;
    
    public Text fishesCaughtText;
    public Animator anim;

    private void Awake()
    {
        master = FindObjectOfType<FishMaster>();
        data = FindObjectOfType<FishData>();
        anim = GetComponent<Animator>();

        wfeof = new WaitForEndOfFrame();
        fishesInNet = new List<GameObject>();
    }

    private void OnEnable()
    {
        moveSpeed = data.shipSpeed;
        rotationSpeed = moveSpeed * 2;
        netObj.localScale = new Vector3(data.netWidth, 0.2f, 0.5f);
        targetPos = transform.position;
        pullingNetUp = false;
        data.fishCaught = 0;
    }

    private void Update()
    {
        if (!master.dayEnded)
        {
            if (Input.touchCount > 0)
            {
                anim.Play("ShipRowing");
                targetPos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
                Rotate();
            }
            else if (Input.GetMouseButton(0))
            {
                anim.Play("ShipRowing");
                targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Rotate();
            }

            if (Input.GetKeyDown("space"))
                StartCoroutine(PullNet());
        }
    }

    private Vector3 diff;
    private float rot_z;
    private void Rotate()
    {
        diff = targetPos - transform.position;
        //diff.Normalize();

        rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, rot_z - 90), Time.deltaTime * rotationSpeed);
    }

    public void CollectFishes()
    {
        if (!pullingNetUp)
            StartCoroutine(PullNet());
    }

    public IEnumerator PullNet()
    {
        pullingNetUp = true;
        float t = 0;
        Vector3 netPos = net.localPosition;
        float startPos = netPos.y;
        Vector3 netScale = net.localScale;
        float startWidth = net.localScale.x;
        float endWidth = 0.5f;

        while (t < 1)
        {
            t += Time.deltaTime;
            netPos.y = Mathf.Lerp(startPos, netCollectPosY, t);
            net.localPosition = netPos;
            netScale.x = Mathf.Lerp(startWidth, endWidth, t);
            net.localScale = netScale;
            yield return wfeof;
        }

        for(int i = 0; i < catchCount; i++)
        {
            data.fishCaught++;
            fishesCaughtText.text = "Päivän saalis: " + data.fishCaught.ToString();
            yield return wfeof;
        }

        for(int i = fishesInNet.Count - 1; i >= 0; i--)
        {
            //Destroy(fishesInNet[i]);
            master.PoolFish(fishesInNet[i]);
        }
        fishesInNet = new List<GameObject>();
        yield return wfeof;

        t = 0;
        while (t < 1)
        {
            t += Time.deltaTime;
            netPos.y = Mathf.Lerp(netCollectPosY, startPos, t);
            net.localPosition = netPos;
            netScale.x = Mathf.Lerp(endWidth, startWidth, t);
            net.localScale = netScale;
            yield return wfeof;
        }

        catchCount = 0;
        pullingNetUp = false;
    }

    private void FixedUpdate()
    {
        if (!master.dayEnded)
        {
            if (Input.GetMouseButton(0))
            {
                //rb2d.velocity = transform.up * moveSpeed;
                if (Vector3.Distance(transform.position, new Vector3(targetPos.x, targetPos.y, 0)) < 0.5f)
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(targetPos.x, targetPos.y, 0), Time.deltaTime * moveSpeed);
                }
                else
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetPos.x, targetPos.y, 0), Time.deltaTime * moveSpeed);
            }                
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!pullingNetUp && collision.CompareTag("Fish") && catchCount < data.netCapacity)
        {
            catchCount++;
            collision.GetComponent<Rigidbody2D>().simulated = false;
            collision.transform.SetParent(net);
            fishesInNet.Add(collision.gameObject);
        }
    }

    public void ApplyUpgrades()
    {

    }
}