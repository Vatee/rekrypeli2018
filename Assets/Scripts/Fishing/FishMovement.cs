﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovement : MonoBehaviour
{
    private FishMaster master;
    private Rigidbody2D rb2d;

    private Vector2 startPos;
    private Vector3 targetPos;
    private Vector3 leavePos;

    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private float rotationSpeed;

    private Vector3 diff;
    float rot_z;

    private void Awake()
    {
        master = FindObjectOfType<FishMaster>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        startPos = transform.position;
        targetPos = transform.position;
        if (startPos.x > 0)
            leavePos = new Vector3(12f, startPos.y, 0f);
        else
            leavePos = new Vector3(-12f, startPos.y, 0f);
    }

    private void Update()
    {
        UpdatePos();
    }

    public void UpdatePos()
    {
        if (!gameObject.activeSelf)
            return;

        if(master.dayEnded)
            targetPos = leavePos;
        else
            targetPos = startPos + Random.insideUnitCircle;

        diff = targetPos - transform.position;
        diff.Normalize();

        rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, rot_z - 90), Time.deltaTime * rotationSpeed);

        if(master.dayEnded)
            rb2d.velocity = transform.up * moveSpeed * 3f;
        else
            rb2d.velocity = transform.up * moveSpeed;

        //if (Mathf.Abs(transform.position.x) >= 12f)
        //    master.PoolFish(gameObject);
    }
}