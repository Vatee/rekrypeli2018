﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTest : MonoBehaviour
{
    public SpriteRenderer spriteRend;
    public Sprite[] sprites;
    public float changeInterval;
    public WaitForSeconds wfs;
    private int counter;
	
	void Start()
	{
        wfs = new WaitForSeconds(changeInterval);
        StartCoroutine(Animate());
	}

    IEnumerator Animate()
    {
        counter = 0;
        for(;;)
        {
            spriteRend.sprite = sprites[counter++];
            if (counter == sprites.Length)
                counter = 0;
            yield return wfs;
        }
    }
}