﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AreaLimits
{
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
}

public class FishMaster : MonoBehaviour
{
    private FishData data;
    private FishClock clock;
    private FishControls fishControls;
    private ShipControls shipControls;

    public int fishCount;    
    public AreaLimits areaLimits;

    [SerializeField]
    private float fishDailyMultiplier;
    private List<GameObject> fishes;
    private Stack<GameObject> fishesPool;
    [SerializeField]
    private int initialPoolSize;
    private GameObject tempFish;
    public GameObject fishPrefab;
    public Transform fishParent;
    public int activeCount;
    public int bigFishCount;

    public int dayEndTime;
    public bool dayEnded;
    public GameObject endDayPanel;
    public Image blackScreen;

    private float dayTime;

    public float dayStartTime;
    public Vector3 playStartPos;

    public WaitForEndOfFrame waitEndFrame = new WaitForEndOfFrame();

    public Text saalisResults;
    public Text pointsResults;
    public RectTransform ratingStars;


    public GameObject introPanel;
    

    private void Start()
    {
        Time.timeScale = 0;
        if (!introPanel.activeSelf)
            introPanel.SetActive(true);

        data = FindObjectOfType<FishData>();
        clock = FindObjectOfType<FishClock>();
        fishControls = FindObjectOfType<FishControls>();
        shipControls = FindObjectOfType<ShipControls>();

        dayEnded = false;
        endDayPanel.SetActive(false);

        fishControls.fishes = new List<FishMovement>();
        fishes = new List<GameObject>();
        fishesPool = new Stack<GameObject>();
        activeCount = 0;

        for (int i = 0; i < initialPoolSize; i++)
        {
            tempFish = Instantiate(fishPrefab, fishParent);
            tempFish.SetActive(false);
            fishesPool.Push(tempFish);
            fishControls.fishes.Add(tempFish.GetComponent<FishMovement>());
        }

        clock.timePassed = dayStartTime;
        SpawnFishes();
    }

    private void Update()
    {
        if (clock.timePassed > dayEndTime && !dayEnded)
        {
            StartCoroutine(EndDay());
        }

    }

    public void StartDayBtn()
    {
        StartCoroutine(StartDay());
    }

    public IEnumerator ClearLevel()
    {
        for(;;)
        {

            yield return waitEndFrame;
        }
    }

    public IEnumerator EndDay()
    {
        dayEnded = true;
        print("Day ended!");
        StartCoroutine(shipControls.PullNet());

        while (shipControls.pullingNetUp)
        {
            yield return new WaitForSeconds(0.5f);
        }

        saalisResults.text = data.fishCaught.ToString() + " kalaa";
        float points = (data.fishCaught * 15);
        pointsResults.text = points.ToString();
        int starsMaxWidth = Mathf.RoundToInt(ratingStars.sizeDelta.x);

        int starsWidth = Mathf.RoundToInt(points);
        if (starsWidth > 500)
            starsWidth = 500;

        ratingStars.sizeDelta = new Vector2(starsWidth, ratingStars.sizeDelta.y);
        print("starsWidth: " + starsWidth + "/" + starsMaxWidth);

        endDayPanel.SetActive(true);
        print(data.fishCaught * 5 + " coins to spawn");

        yield return waitEndFrame;
    }

    public IEnumerator StartDay()
    {
        float t = 0;
        Color tempClr = blackScreen.color;
        blackScreen.enabled = true;
        while (t < 1) //Make screen black
        {
            t += Time.deltaTime;
            tempClr.a = t;
            blackScreen.color = tempClr;
            yield return waitEndFrame;
        }

        //Reset stuff for new day
        clock.timePassed = dayStartTime;
        shipControls.transform.position = playStartPos;
        data.fishCaught = 0;
        shipControls.fishesCaughtText.text = "Päivän saalis: " + data.fishCaught.ToString();
        shipControls.ApplyUpgrades();
        SpawnFishes();

        while (t > 1) //Remove black screen
        {
            t -= Time.deltaTime;
            tempClr.a = t;
            blackScreen.color = tempClr;
            yield return waitEndFrame;
        }
        blackScreen.enabled = false;
    }

    private void SpawnFishes()
    {
        /*for (int i = fishes.Count - 1; i > 0; i--)
        {
            tempFish = fishes[i];
            fishes.RemoveAt(i);
            tempFish.SetActive(false);
            fishesPool.Push(tempFish);
            activeCount--;
        } */

        for (int i = 0; i < fishCount; i++)
        {
            if (fishesPool.Count > 0)
                tempFish = fishesPool.Pop();
            else
            {
                tempFish = Instantiate(fishPrefab, fishParent);
                fishControls.fishes.Add(tempFish.GetComponent<FishMovement>());
            }

            tempFish.SetActive(true);
            tempFish.transform.position = new Vector3(Random.Range(areaLimits.minX, areaLimits.maxX), Random.Range(areaLimits.minY, areaLimits.maxY), 0);
            if (bigFishCount > 0)
            {
                float fishSize = Random.Range(0.3f, 0.6f);
                tempFish.transform.localScale = new Vector3(fishSize, fishSize, fishSize);
                bigFishCount--;
            }
            fishes.Add(tempFish);
            activeCount++;
        }
    }

    public void PoolFish(GameObject fish)
    {
        fish.transform.SetParent(fishParent);
        fish.SetActive(false);
        fishes.Remove(fish);
        fishesPool.Push(fish);
    }

    public void StartGame()
    {
        Time.timeScale = 1;
        introPanel.SetActive(false);
    }

    public void CloseGame()
    {
        if (MinigamesMaster.GetInstance)
            MinigamesMaster.GetInstance.CloseScene();
    }
}