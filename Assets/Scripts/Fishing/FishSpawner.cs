﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
    //private FishMaster fishMaster;
    private FishControls fishControls;

    [SerializeField]
    private float maxSpawnFreq;
    [SerializeField]
    private Vector2 spawnPos;
    private Vector2 spawnVelocity;
    [SerializeField]
    private GameObject fish;

    private float counter;
    [SerializeField]
    private List<GameObject> fishes;
    private Stack<GameObject> fishesPool;
    [SerializeField]
    private int initialPoolSize;
    private GameObject tempFish;

    private void Awake()
    {
        //fishMaster = FindObjectOfType<FishMaster>();
        fishControls = FindObjectOfType<FishControls>();
    }

    private void Start()
    {
        fishes = new List<GameObject>();
        fishesPool = new Stack<GameObject>();
        fishControls.fishes = new List<FishMovement>();

        for (int i = 0; i < initialPoolSize; i++)
        {
            tempFish = Instantiate(fish);
            tempFish.SetActive(false);
            fishesPool.Push(tempFish);
            fishControls.fishes.Add(tempFish.GetComponent<FishMovement>());
        }
    }

    void Update()
    {
        counter += Time.deltaTime;

        if (counter > maxSpawnFreq)
        {
            counter -= maxSpawnFreq;
            spawnPos.y = Random.Range(-4.4f, 0.75f);

            if(fishesPool.Count > 0)
                tempFish = fishesPool.Pop();
            else
            {
                tempFish = Instantiate(fish);
                fishControls.fishes.Add(tempFish.GetComponent<FishMovement>());
            }

            tempFish.SetActive(true);
            tempFish.transform.position = spawnPos;
            spawnVelocity.x = Random.Range(1f, 4f);
            tempFish.GetComponent<Rigidbody2D>().velocity = spawnVelocity;
            fishes.Add(tempFish);
        }

        for(int i = fishes.Count - 1; i > 0; i--)
        {
            if(fishes[i].transform.position.x > 10)
            {
                fishes[i].SetActive(false);
                fishesPool.Push(fishes[i]);
                fishes.RemoveAt(i);
            }
        }
    }
}