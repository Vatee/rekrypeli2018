﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FishClock : MonoBehaviour
{
    private FishMaster master;

    public float timePassed;
    public float minutesPassed;
    public float timeScale;
    public Text clockText;

    private string hour;
    private string minute;

	void Start ()
	{
        master = FindObjectOfType<FishMaster>();
	}
	
	void Update ()
	{
        if(!master.dayEnded)
        {
            timePassed += Time.deltaTime * timeScale;
            //minutesPassed += Time.deltaTime * timeScale / 60f;

            //if (minutesPassed >= 60f)
            //    minutesPassed -= 60f;

            minute = Mathf.FloorToInt((timePassed - (Mathf.Floor(timePassed / 3600f) * 3600f)) / 60f).ToString();
            hour = Mathf.FloorToInt(timePassed / 3600f).ToString();

            if (minute.Length == 1)
                minute = "0" + minute;
            if (hour.Length == 1)
                hour = "0" + hour;

            clockText.text = hour + ":" + minute;
        }
	}
}
