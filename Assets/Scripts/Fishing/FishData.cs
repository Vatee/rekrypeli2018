﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishData : MonoBehaviour
{
    public int money;
    public int fishCaught;
    public float shipSpeed;
    public float netWidth;
    public int netCapacity;
}