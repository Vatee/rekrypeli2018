﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundSwapper : Singleton<BackgroundSwapper> {
    public bool useRandomBackground;
    public Texture[] images;
    public RawImage background;

	public void SwapBackground(int imgID)
    {
        background.texture = images[imgID];
    }

    public void RandomBackground()
    {
        if (useRandomBackground)
            background.texture = images[Mathf.RoundToInt(Random.Range(0, images.Length))];
    }

    private void Awake()
    {
        if (useRandomBackground)
            RandomBackground();
    }
}
