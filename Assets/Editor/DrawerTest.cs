﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(KoulAlaValinta))]
public class DrawerTest : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        //EditorGUI.indentLevel = 0;

        // Calculate rects
        //Rect amountRect = new Rect(position.x, position.y, 30, position.height);
        Rect nameRect = new Rect(position.x, position.y, (EditorGUIUtility.currentViewWidth - position.x - 40)/2, position.height);        
        Rect unitRect = new Rect(nameRect.xMax, position.y, (EditorGUIUtility.currentViewWidth - position.x - 40) / 2, position.height);


        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("amount"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("koulutusala"), GUIContent.none);
        EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("amount"), GUIContent.none);
        

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
    /*
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (property.isExpanded)
            return EditorGUI.GetPropertyHeight(property) + 10;
        else
            return EditorGUI.GetPropertyHeight(property) + 10;
    } */
}



