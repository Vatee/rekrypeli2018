﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
public class EnumFlagDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EnumFlagAttribute flagSettings = (EnumFlagAttribute)attribute;
        //Enum targetEnum = (Enum)fieldInfo.GetValue(property.serializedObject.targetObject);
        Enum targetEnum = (Enum)Enum.ToObject(fieldInfo.FieldType, property.intValue);

        string propName = flagSettings.name;
        if (string.IsNullOrEmpty(propName))
            propName = ObjectNames.NicifyVariableName(property.name);
        

        EditorGUI.BeginProperty(position, label, property);
        Enum enumNew = EditorGUI.EnumFlagsField(position, GUIContent.none, targetEnum);
        property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());
       
        EditorGUI.EndProperty();
    }
}