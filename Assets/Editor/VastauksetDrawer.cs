﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditorInternal;
using UnityEditor;

//[CustomPropertyDrawer(typeof(Vastaus))]
public class VastauksetDrawer : PropertyDrawer {
    float height1;
    float height2;

    string test1;
    string test2;
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        var indent = EditorGUI.indentLevel;
        //EditorGUI.indentLevel = -20;

        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        

        // Calculate rects
        //Rect amountRect = new Rect(position.x, position.y, 30, position.height);
        Rect nameRect = new Rect(position.x + 2, position.y, EditorGUIUtility.currentViewWidth - 150, EditorGUIUtility.singleLineHeight);
        Rect unitRect = new Rect(position.x, position.y, EditorGUIUtility.currentViewWidth - 150, EditorGUIUtility.singleLineHeight);


        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        //EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("amount"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("vastaus"), GUIContent.none);
        EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("kAla"), GUIContent.none, true);


        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }    


    
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property.FindPropertyRelative("kAla"), GUIContent.none, true);
    } 
} 



