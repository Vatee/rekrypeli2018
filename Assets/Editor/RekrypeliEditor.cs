﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.IO;

public class ReorderableListChild
{
    public ReorderableList reList;
    public List<float> heights = new List<float>(30);
}

[CustomEditor(typeof(Rekrypeli))]
public class RekrypeliEditor : Editor {
    public QuestionsAsset questionsAsset;

    private ReorderableList reList;
    private List<ReorderableListChild> reListChilds = new List<ReorderableListChild>();

    private Vector2 scrollViewPos;
    private float oldScrollPos;

    bool modified;

    public override void OnInspectorGUI()
    {
        
        DrawDefaultInspector();

        EditorGUI.BeginChangeCheck();
        serializedObject.Update();

        scrollViewPos = EditorGUILayout.BeginScrollView(scrollViewPos, GUILayout.Width(EditorGUIUtility.currentViewWidth - 15), GUILayout.Height(500));
        reList.DoLayoutList();
        EditorGUILayout.EndScrollView();

        if (EditorGUI.EndChangeCheck())
        {
            //Debug.Log(GetInspectorScrollPos());
            serializedObject.ApplyModifiedProperties();
            modified = true;
        }
        
        if (oldScrollPos != scrollViewPos.y)
        {
            Rect hiddenRect = new Rect(0, 0, 0, 0);
            GUI.SetNextControlName("test");
            EditorGUI.TextField(hiddenRect, "NotLoaded");
            GUI.FocusControl("test");
        } 

        oldScrollPos = scrollViewPos.y;
        /*
        //Rekrypeli rekryScript = (Rekrypeli)target;
        if (GUILayout.Button("Save"))
        {
            SaveToAssets();
        }

        if (GUILayout.Button("Load"))
        {
            LoadFromAssets();
        } */

        if (GUILayout.Button("Save to txt"))
        {
            TestWrite();
        }
    }

    
    List<float> heights = new List<float>();
    List<bool> initialized = new List<bool>();
    List<bool> folds = new List<bool>();
    List<SerializedProperty> elements = new List<SerializedProperty>();
    List<string> names = new List<string>();
    List<int> tiers = new List<int>();

    public void OnEnable()
    {
        Rekrypeli rekryScript = (Rekrypeli)target;
        reList = new ReorderableList(serializedObject, serializedObject.FindProperty("kysymykset"), true, true, true, true);
        
        reList.drawHeaderCallback = rect =>
        {
            EditorGUI.LabelField(rect, "Kysymykset", EditorStyles.boldLabel);
        }; 

        heights = new List<float>();
        initialized = new List<bool>();
        folds = new List<bool>();
        elements = new List<SerializedProperty>();

        reList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
          
            int offsetY = 4;

            if (elements.Count <= index)
            {
                elements.Insert(index, reList.serializedProperty.GetArrayElementAtIndex(index));
                initialized.Insert(index, false);
                heights.Insert(index, EditorGUIUtility.singleLineHeight + offsetY);
                folds.Insert(index, false);
                reListChilds.Insert(index, new ReorderableListChild());
            }

            SerializedProperty element = elements[index];
            if (!initialized[index])
            {
                names.Insert(index, element.FindPropertyRelative("kysymys").stringValue);
                tiers.Insert(index, element.FindPropertyRelative("tier").intValue);
                reListChilds[index].reList = new ReorderableList(serializedObject, element.FindPropertyRelative("vastaukset"), true, true, true, true);
                CreateChildReList(element.FindPropertyRelative("vastaukset").displayName, index);
                initialized[index] = true;
            }

            heights[index] = EditorGUIUtility.singleLineHeight + offsetY;

            float scrollPos = scrollViewPos.y;
            if (500 + scrollPos > rect.yMin && scrollPos < rect.yMax)
            {
                Rect checkboxRect = new Rect(rect.x + 12, rect.y + offsetY / 2, 15, EditorGUIUtility.singleLineHeight);
                //Rect usedRect = new Rect(checkboxRect.xMax, rect.y + offsetY / 2, 15, EditorGUIUtility.singleLineHeight);                
                Rect tierRect = new Rect(checkboxRect.xMax + 1.5f, rect.y + offsetY / 2, 15, EditorGUIUtility.singleLineHeight);
                Rect nameRect = new Rect(tierRect.xMax, rect.y + offsetY / 2, EditorGUIUtility.currentViewWidth - 180, EditorGUIUtility.singleLineHeight);
                Rect kalaRect = new Rect(nameRect.xMax, rect.y + offsetY / 2, 70, EditorGUIUtility.singleLineHeight);
                Rect vastRect = new Rect(rect.x + 10, rect.y + offsetY / 2, 10, EditorGUIUtility.singleLineHeight);
                Rect vastCont = new Rect(rect.x + 20, rect.y + EditorGUIUtility.singleLineHeight + offsetY, EditorGUIUtility.currentViewWidth - 90, EditorGUIUtility.singleLineHeight);

                //EditorGUI.PropertyField(usedRect, element.FindPropertyRelative("used"), GUIContent.none);
                EditorGUI.PropertyField(checkboxRect, element.FindPropertyRelative("monivalinta"), GUIContent.none);
                EditorGUI.PropertyField(kalaRect, element.FindPropertyRelative("kAla"), GUIContent.none);
                

                if (isActive || isFocused)
                {
                    SerializedProperty kysymys = element.FindPropertyRelative("kysymys");
                    SerializedProperty tier = element.FindPropertyRelative("tier");
                    EditorGUI.PropertyField(nameRect, kysymys, GUIContent.none);
                    EditorGUI.PropertyField(tierRect, tier, GUIContent.none);

                    if (modified)
                    {
                        if (names[index] != kysymys.stringValue)
                        {
                            names[index] = kysymys.stringValue;
                            modified = false;
                        }

                        if (tiers[index] != tier.intValue)
                        {
                            tiers[index] = tier.intValue;
                            modified = false;
                        }
                    }
                } 
                else
                {
                    EditorGUI.LabelField(nameRect, names[index]);
                    EditorGUI.LabelField(tierRect, tiers[index].ToString());
                }

                if (isFocused && Event.current.type == EventType.MouseDrag)
                folds[index] = false;

                folds[index] = EditorGUI.Foldout(vastRect, folds[index], GUIContent.none);

                if (folds[index])
                {
                    if (initialized[index])
                    {
                        reListChilds[index].reList.DoList(vastCont);
                        heights[index] = EditorGUIUtility.singleLineHeight + reListChilds[index].reList.GetHeight() + offsetY * 2;
                    }
                }
            } 
     


        };      
        
        reList.elementHeightCallback = (index) =>
        {
            //var element = reList.serializedProperty.GetArrayElementAtIndex(index);

            if (heights.Count <= index)
                heights.Insert(index, EditorGUIUtility.singleLineHeight);

            return heights[index];
        };

        

    }

    private void CreateChildReList(string listName, int indx)
    {
        reListChilds[indx].reList.drawHeaderCallback = rect =>
        {
            EditorGUI.LabelField(rect, listName, EditorStyles.boldLabel);
        };

        reListChilds[indx].reList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = reListChilds[indx].reList.serializedProperty.GetArrayElementAtIndex(index);
            int offsetY = 4;

            if (reListChilds[indx].heights.Count == index)
                reListChilds[indx].heights.Insert(index, EditorGUIUtility.singleLineHeight);

            Rect nameRect = new Rect(rect.x + 20, rect.y + offsetY / 2, EditorGUIUtility.currentViewWidth - rect.x - 70, EditorGUIUtility.singleLineHeight);
            Rect vastRect = new Rect(rect.x + 15, rect.y + offsetY / 2, EditorGUIUtility.currentViewWidth - rect.x - 65, EditorGUIUtility.singleLineHeight);

            EditorGUI.PropertyField(nameRect, element.FindPropertyRelative("vastaus"), GUIContent.none);
            EditorGUI.PropertyField(vastRect, element.FindPropertyRelative("kAla"), GUIContent.none, true);

            reListChilds[indx].heights[index] = EditorGUI.GetPropertyHeight(element.FindPropertyRelative("kAla"), GUIContent.none, true) + offsetY;
        };

        reListChilds[indx].reList.elementHeightCallback = (index) =>
        {
            if (reListChilds[indx].heights.Count == index)
                reListChilds[indx].heights.Insert(index, EditorGUIUtility.singleLineHeight);

            return reListChilds[indx].heights[index];
        };
    }


    private void SaveToAssets()
    {
        Rekrypeli rekryScript = (Rekrypeli)target;
        questionsAsset = ScriptableObject.CreateInstance<QuestionsAsset>();
        questionsAsset.kysymysLista = rekryScript.kysymykset;
        //string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/Scenes/SceneSetup.asset");

        string assetPathAndName = ("Assets/QuestionsAsset.asset");
        AssetDatabase.DeleteAsset(assetPathAndName);
        AssetDatabase.CreateAsset(questionsAsset, assetPathAndName);
        EditorUtility.SetDirty(questionsAsset);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        Debug.Log("Saved " + rekryScript.kysymykset.Length + " kysymystä");
    }

    private void LoadFromAssets()
    {        
        questionsAsset = AssetDatabase.LoadAssetAtPath<QuestionsAsset>("Assets/QuestionsAsset.asset");
        Rekrypeli rekryScript = (Rekrypeli)target;
        if (questionsAsset)
        {
            rekryScript.kysymykset = questionsAsset.kysymysLista;
            Debug.Log("Loaded " + questionsAsset.kysymysLista.Length + " kysymstä");
        }
        else
            Debug.Log("questionsAsset is null");

    }

    private void TestWrite()
    {
        string path = "Assets/kysymykset.txt";

        StreamWriter writer = new StreamWriter(path, false);
        Rekrypeli rekryScript = (Rekrypeli)target;
        foreach (Kysymys kys in rekryScript.kysymykset)
        {            
            writer.WriteLine("Kysymys: " + kys.kysymys.ToString());
            writer.WriteLine("  Koulutusala: " + kys.kAla.ToString());
            writer.WriteLine(" ");
            writer.WriteLine("Vastaukset: ");
            foreach (Vastaus vst in kys.vastaukset)
            {
                writer.WriteLine("  " + vst.vastaus.ToString());
                foreach (KoulAlaValinta kav in vst.kAla)
                {
                    writer.WriteLine("    " + kav.koulutusala + ": " + kav.amount + " pistettä");
                }
                writer.WriteLine(" ");
            }
            writer.WriteLine(" ");
        }
        
        writer.Close();        
    }
}
